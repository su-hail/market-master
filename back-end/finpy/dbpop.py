from finpy.models import Stock
import csv


with open("small.csv", "wb") as csvfile:
    for line in csvfile:
        ticker_list = line.split(",")

        stock = Stock( ticker = line[0], name  = line [1], last_price = line[2], sector = line[5], market_cap = line[3])
        stock.save()
