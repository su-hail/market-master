from __future__ import unicode_literals
from django.db import models

class Stock(models.Model):
    ticker = models.CharField(max_length = 10)
    name = models.CharField(max_length = 128)
    last_price = models.IntegerField(default = 0)
    market_cap = models.IntegerField(default = 0)
    sector = models.CharField(max_length = 128)

    def __unicode__(self):
        return self.ticker 


    # install class Meta if you want some additional ordering of the instances
