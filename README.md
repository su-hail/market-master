This is a financial education app that is geared to teach young investors different strategies they
can use to make educated decisions in the stock market. 

Other websites like "How the Market Works" do a great job simulating the marekt, but do not offer
real educational content that actually give people a better understanding of why and how the markets
move. 


This app is strucutred to be like a Khan Academy for investment education.
Many of the backend features such as the stock screener and simulator have been completed at this point
However, several others such as the automated stock suggestions are still in the works.
Furthermore, front end and educational content are still in progress.



OUR TEAM 
---------------------- 
Rishub Nahar - Founder and Backend developer 

Suhail Jawahardeen - Content developer 

Dean Foster - Front end developer 

Expected to be released by end of summer 2017 
